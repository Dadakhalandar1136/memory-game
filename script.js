const imagePath1 = 'gifs/1.gif';
const imagePath2 = 'gifs/2.gif';
const imagePath3 = 'gifs/3.gif';
const imagePath4 = 'gifs/4.gif';
const imagePath5 = 'gifs/5.gif';
const imagePath6 = 'gifs/6.gif';
const imagePath7 = 'gifs/7.gif';
const imagePath8 = 'gifs/8.gif';
const imagePath9 = 'gifs/9.gif';
const imagePath10 = 'gifs/10.gif';
const imagePath11 = 'gifs/11.gif';
const imagePath12 = 'gifs/12.gif';
const avengersImagePath = 'gifs/avengers.jpg';

const game = document.getElementById('game');
const startGame = document.getElementById('start-game');
const mainContainer = document.querySelector('.main-container');
const selectLevel = document.querySelector('.select-level');
const easyLevel = document.getElementById('easy-level');
const mediumLevel = document.getElementById('medium-level');
const hardLevel = document.getElementById('hard-level');

let count = 0;
let firstClick = '';
let secondClick = '';
let moves = 0;
let success = 0;
let maxSuccess;

function handleStartGame(event) {
  event.target.parentElement.style.display = 'none';
  selectLevel.style.display = 'flex';
}

startGame.addEventListener('click', handleStartGame);

function handleEasyLevel(event) {
  event.target.parentElement.style.display = 'none';
  mainContainer.style.display = 'flex';
  const imagesOne = [
    imagePath1,
    imagePath1,
    imagePath2,
    imagePath2,
    imagePath3,
    imagePath3,
    imagePath4,
    imagePath4,
    imagePath5,
    imagePath5,
    imagePath6,
    imagePath6
  ];
  maxSuccess = 6;
  let shuffledImagesOne = shuffle(imagesOne);
  createDivsForImages(shuffledImagesOne);
}

easyLevel.addEventListener('click', handleEasyLevel);

function handleMediumLevel(event) {
  event.target.parentElement.style.display = 'none';
  mainContainer.style.display = 'flex';
  const imagesTwo = [
    imagePath1,
    imagePath1,
    imagePath2,
    imagePath2,
    imagePath3,
    imagePath3,
    imagePath4,
    imagePath4,
    imagePath5,
    imagePath5,
    imagePath6,
    imagePath6,
    imagePath7,
    imagePath7,
    imagePath8,
    imagePath8
  ];
  maxSuccess = 8
  let shuffledImagesTwo = shuffle(imagesTwo);
  createDivsForImages(shuffledImagesTwo);
}

mediumLevel.addEventListener('click', handleMediumLevel);

function handleHardLevel(event) {
  event.target.parentElement.style.display = 'none';
  mainContainer.style.display = 'flex';
  const imagesThree = [
    imagePath1,
    imagePath1,
    imagePath2,
    imagePath2,
    imagePath3,
    imagePath3,
    imagePath4,
    imagePath4,
    imagePath5,
    imagePath5,
    imagePath6,
    imagePath6,
    imagePath7,
    imagePath7,
    imagePath8,
    imagePath8,
    imagePath9,
    imagePath9,
    imagePath10,
    imagePath10,
    imagePath11,
    imagePath11,
    imagePath12,
    imagePath12
  ];
  maxSuccess = 12;
  let shuffledImagesThree = shuffle(imagesThree);
  createDivsForImages(shuffledImagesThree);
}

hardLevel.addEventListener('click', handleHardLevel);


// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}
// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForImages(imagesArray) {

  for (let image of imagesArray) {
    // create a new div
    const newDiv = document.createElement('div');
    
    const frontFace = document.createElement('img');
    frontFace.classList.add('front-face');
    frontFace.src = image;
    
    const backFace = document.createElement('img');
    backFace.classList.add('back-face');
    backFace.src = avengersImagePath;

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(image.slice(0, 7).replace('/', '-').replace('.', ''));
    newDiv.classList.add('memory-card');
    newDiv.dataset.imageId = 0;
    newDiv.append(frontFace, backFace);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener('click', handleCardClick);

    // append the div to the element with an id of game
    game.append(newDiv);
  }
}

// TODO: Implement this function!

function handleCardClick() {
  let highScore = document.getElementById('high-score');
  let movesElement = document.getElementById('moves');
  let successContainer = document.querySelector('.success-container');
  let scoreCard = document.getElementById('score');
  let totalScore = document.getElementById('total-score');
  
  if (count < 2) {
    
    if (this.dataset.imageId == 0) {
      moves++;
      this.classList.toggle('flip');
      this.dataset.imageId = 1;
      movesElement.textContent = `Moves: ${moves}`;
      if (count === 0) {
        firstClick = this;
        count++;
      } else {
        secondClick = this;
        count++;
      }
      
      if (secondClick) {
        if (firstClick.classList[0] === secondClick.classList[0]) {
          success++;
          count = 0;
        } else {
          firstClick.dataset.imageId = 1;
          if (count === 2) {
            setTimeout(() => {
              firstClick.dataset.imageId = 0;
              secondClick.dataset.imageId = 0;
              firstClick.classList.remove('flip');
              secondClick.classList.remove('flip');
              count = 0;
              secondClick = '';
            }, 1 * 1000);
          }
        }
      }
      
      scoreCard.textContent = `Score: ${Math.round((success / (moves / 2)) * 100)}`;
      
      if (success === maxSuccess) {
        mainContainer.style.display = 'none';
        successContainer.style.display = 'block';
        let finalScore = Math.round((success / (moves / 2)) * 100);
        totalScore.textContent = `Total Score: ${finalScore}`;
        let newHighScore = updateLocalStorage(finalScore);
        highScore.textContent = `High Score: ${newHighScore}`;
      }
    }
  }
}

function updateLocalStorage(finalScore) {
  let newHighScore;
  if (localStorage.getItem('high_score') === null) {
    localStorage.setItem('high_score', finalScore);
    newHighScore = finalScore;
  } else {
    newHighScore = localStorage.getItem('high_score');
    if (newHighScore < finalScore) {
      localStorage.setItem('high_score', finalScore);
      newHighScore = finalScore;
    }
  }
  return newHighScore;
}